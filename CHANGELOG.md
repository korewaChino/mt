# Changelog

I only started tracking this on January 27th, 2021 so a lot of changes aren't logged.

## v0.1.0+alpha2021.07.11
- Fully migrated main window to libhandy TabView and TabBar
- Reconnected all gactions for window
- Moved from git repos to crates.io
- New trait `Layout` to allow for multiple window layout configurations (only unified is implemented)
- Some bugs are present since migrating, will fix

## v0.1.0+alpha2021.06.19
- Updated to latest master of gtk3 and libhandy
- Minimal work on allowing multiple layouts for windows
- Will port notebook to libhandy::TabView and libhandy::TabBar
- Adapted code to use the new gtk3-rs APIs

## v0.1.0+alpha2021.05.29
- Rebuilt the menu to be more adaptive and added full multi-profile support (for tabs)
- Now the window adapts to the colorscheme of the active terminal
- Small update for the most part, haven't had much time (or motivation for that matter)

## v0.1.0+alpha2021.05.05
- Added new message passing system to centralize window messages into a event loop
- Overhauled styling system, once multiple profiles are enabled, should allow entire window to be recolored to match colorscheme
- General refactoring
- I actually forget everything I've done in the past month and I did skip the changelog for a version or two. 

## v0.1.0+alpha2021.03.28
- Complete overhaul of the `Config` and `Profile` systems
- Redid internal app structure for managing profiles and colorschemes
- Unified config and profile into single file
- Multiple profiles are now loaded from config, althrough there still isn't any way to use more than one profile at a time
- All profiles now inherit from `config.profiles.defaults`, which contains fallback values for undefined `Profile` fields

## v0.1.0+alpha2021.03.15
- New icons, new sized and symbolic
- Restructured gresources
- Redid preferences to use ui files
- Move window specific actions out of `MtApplication` and into `MtApplicationWindow`
- Cleaned up old config and colorscheme files

## v0.1.0+alpha2021.03.06
- New icons and banner (again)
- More work on preferences dialog
    - Learning about gio::ListModel
    - Added a few new fields
    - Removed a page

## v0.1.0+alpha2021.02.24
- Replaced preferences window with `HdyPreferencesWindow`
- Began preliminary work within application to support config files
- Refactored `MtApplicationPriv::get_profile()`
- Added `-p/--profile` and `-c/--config` command line options
- Formatting and documentation
- Added traits `Load` and `LoadMtResource`
- Added overlay scrolling to terminal
- Added new `Error` conversions
- Renamed resource and profile directory to "Mt"
- New icons

## v0.1.0+alpha2021.02.09
- Removed unnecessary conversion to `Path` in `util::parse_cmd()`
## v0.1.0+alpha2021.02.08
- Drastically improved command parsing functionality
    - Command can now have parameters
    - Any environment variable in command will be parsed to their values if they exist, if not they will be left alone
    - Commands do not have to include their full path if they exist within `$PATH`
    - Command themselves can be environment variables, though if that variable is not found it will default to profile-defined
- Added option `-e / --command` to overwrite `Profile.shell.cmd`
- Formatted entire project
- Refactored all parts of application that attempt to parse strings for environment variables
    - Created `util` functions to perform these tasks
- Command line parsing has been cleaned up to be more idiomatic
- Began initial work for global configs, not compiled into `Release`

## v0.1.0+alpha2021.02.03
- Allow `--working-directory` arg to parse and replace environment variables
- Profiles fields `cursor_blink_mode`, `audible_bell`, and `erase_binding`

## v0.1.0+alpha2021.01.30
- Renamed executable from `mt` to `Mt` to avoid conflicts with GNU mt
- Replaced `Profile::verify()` with `Profile::finalize()`
    - Now can replace `n` number of environment variables in both `cwd` and `cmd`
- Changed `Shell.dir` to `Shell.cwd`
- Added `Shell.login_shell` field, currently unimplemented
- Changed default behavior of `Shell::default()`
    - `Shell.cmd` defaults to `$SHELL`
- Added two new `Error` variants
    - `Error::InvalidCmdError` for when a given `cmd` is not found
    - `Error::EnvError` for when an invalid environment variable is found when parsing `cmd` and `cwd`
- Added skeleton of `Config`, currently not implemented

## v0.1.0+alpha2021.01.28
- Changed all `Profile` struct fields from `pub(crate)` to `pub`
- Created new structs to represent font weight and terminal cursor style 
    - `From<CursorShape> for vte::CursorShape` 
    - `From<FontWeight> for pango::Weight`
- Changed profile format from YAML to RON and updated default profile
- Connected font weight and cursor shape properties to vte
- Changed API to interact with `Profile`
- Now all fields of `Profile` represent real values that impact the application


## v0.1.0+alpha2021.01.27
- Update license to GPLv2
- Fully changed name from `Terminal` to `Mt`
- Changed all instances of `com.gitlab.miridyan.Terminal` to `com.gitlab.miridyan.Mt`
- Changed all instances of `/com/gitlab/miridyan/Terminal` to `/com/gitlab/miridyan/Mt` 


## v0.1.0+alpha2021.01.26
- Changed all cli params from `OptionFlags::OPTIONAL_ARG` to `OptionFlags::NONE`
- Added command line options `--working-directory`/`-w` and `--profile`/`-p`
- Added local command line option `--new-process` which sets `G_APPLICATION_NON_UNIQUE`
