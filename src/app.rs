use crate::{
    config::{ColorScheme, Config, Layout, Profile, ScrollbarVisibility},
    traits::*,
    util,
    window::{self, MtApplicationWindow},
};

use gdk::Screen;
use gio::{prelude::*, subclass::prelude::*, Action, ApplicationFlags, ResourceLookupFlags};
use glib::{char::Char, clone, subclass, OptionArg, OptionFlags, Variant, VariantTy, VariantType};
use gtk::{prelude::*, subclass::prelude::*, CssProvider, Settings, StyleContext, Window};
use log::debug;
use once_cell::unsync::OnceCell;
use std::{cell::RefCell, path::PathBuf, rc::Rc, str};
use xdg::BaseDirectories;

#[cfg(debug_assertions)]
pub static APPID: &'static str = "com.gitlab.miridyan.Mt.Devel";
#[cfg(not(debug_assertions))]
pub static APPID: &'static str = "com.gitlab.miridyan.Mt";

#[derive(Clone, Debug)]
pub struct MtApplicationPriv {
    // Need to replace this is a vector for multiwindow support
    window: OnceCell<MtApplicationWindow>,
    settings: Settings,
    base_directories: BaseDirectories,
    config: Rc<RefCell<Config>>,
}

#[derive(Debug)]
pub struct TerminalParams(pub Profile, pub ColorScheme, pub ScrollbarVisibility);

#[glib::object_subclass]
impl ObjectSubclass for MtApplicationPriv {
    const NAME: &'static str = "MtApplication";

    type Type = MtApplication;
    type ParentType = gtk::Application;
    type Class = subclass::basic::ClassStruct<Self>;
    type Instance = subclass::basic::InstanceStruct<Self>;

    fn new() -> Self {
        let (config, base_directories) = util::initialize_environment();
        log::info!("Config {:?}", config);
        Self {
            window: OnceCell::new(),
            settings: Settings::default().unwrap(),
            base_directories,
            config: Rc::new(RefCell::new(config)),
        }
    }
}

impl ObjectImpl for MtApplicationPriv {}

impl ApplicationImpl for MtApplicationPriv {
    fn activate(&self, application: &Self::Type) {
        self.parent_activate(application);

        if let Err(e) = self
            .settings
            .set_property("gtk-application-prefer-dark-theme", true)
        {
            log::error!("Failed to set gtk-application-prefer-dark-theme {:?}", e);
        }

        if let Err(e) = self.load_stylesheets() {
            log::error!("Failed to load stylesheets {:?}", e);
        }
    }

    fn startup(&self, application: &Self::Type) {
        self.parent_startup(application);

        if cfg!(debug_assertions) {
            Window::set_interactive_debugging(true);
        }

        if let None = self.window.get() {
            let app = application.downcast_ref::<MtApplication>().unwrap();

            let window = MtApplicationWindow::new(&app);

            self.window.set(window).unwrap();
            app.setup_gactions();
        }
    }

    fn handle_local_options(&self, application: &Self::Type, options: &glib::VariantDict) -> i32 {
        if let Some(variant) =
            options.lookup_value("new-process", Some(&VariantTy::new("b").unwrap()))
        {
            if let Some(new_process) = variant.get::<bool>() {
                if new_process {
                    application.set_flags(application.flags() | ApplicationFlags::NON_UNIQUE);
                }
            } else {
                return 1;
            }
        }

        if let Some(variant) = options.lookup_value("config", Some(&VariantTy::new("s").unwrap())) {
            if let Some(config) = variant.get::<String>() {
                let pathbuf = PathBuf::from(config);
                let config = self.config.try_borrow_mut();

                if pathbuf.is_file() && config.is_ok() {
                    let res = Config::load_from_path(&pathbuf).and_then(|c| {
                        *config? = c;

                        Ok(())
                    });

                    match res {
                        Ok(_) => log::info!("Successfully found and parsed config"),
                        Err(e) => log::error!("Invalid config : {}", e),
                    }
                }
            }
        }

        self.parent_handle_local_options(application, options)
    }

    fn command_line(
        &self,
        application: &Self::Type,
        command_line: &gio::ApplicationCommandLine,
    ) -> i32 {
        self.activate(application);

        let options_dict = command_line.options_dict();
        let s = Some(VariantTy::new("s").unwrap());
        let cwd = options_dict
            .lookup_value("working-directory", s)
            .and_then(|option| option.get::<String>())
            .and_then(|path| util::parse_path(path));

        let cmd = options_dict
            .lookup_value("command", s)
            .and_then(|option| option.get::<String>())
            .and_then(|cmd| util::parse_cmd(cmd))
            .and_then(|p_cmd| Some(p_cmd.split(" ").map(PathBuf::from).collect::<Vec<_>>()));

        let profile = options_dict
            .lookup_value("profile", s)
            .and_then(|option| option.get::<String>());

        self.new_session(profile.as_ref().map(String::as_str), cwd, cmd);

        let window = self
            .window()
            .expect("Failed to get window, should not occur");

        window.show();
        window.present();
        window.focus_active_tab();

        0
    }
}

impl GtkApplicationImpl for MtApplicationPriv {}

impl MtApplicationPriv {
    pub fn window(&self) -> Option<&MtApplicationWindow> {
        self.window.get()
    }

    fn load_stylesheets(&self) -> crate::error::Result<()> {
        let provider = CssProvider::new();
        let screen = Screen::default().unwrap();
        let mut stylesheet = str::from_utf8(&*gio::resources_lookup_data(
            "/com/gitlab/miridyan/Mt/theme/base.css",
            ResourceLookupFlags::NONE,
        )?)?
        .to_string();

        if let Some(colorschemes) = self.build_stylesheets() {
            stylesheet.push_str("\n\n");
            stylesheet.push_str(&colorschemes);
        }

        provider.load_from_data(stylesheet.as_bytes())?;
        StyleContext::add_provider_for_screen(
            &screen,
            &provider,
            gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
        );

        Ok(())
    }

    fn build_stylesheets(&self) -> Option<String> {
        self.config
            .try_borrow()
            .or_else(|e| {
                log::error!("Config borrow failed {:?}", e);
                Err(e)
            })
            .ok()
            .and_then(|config| {
                let stylesheet = std::str::from_utf8(
                    &*gio::resources_lookup_data(
                        "/com/gitlab/miridyan/Mt/theme/colorscheme.css",
                        ResourceLookupFlags::NONE,
                    )
                    .ok()?,
                )
                .ok()?
                .to_string();

                let colorschemes = &config.colorschemes;
                let scheme_sheets = colorschemes
                    .iter()
                    .map(|(key, val)| {
                        let tab = format!("{:06x}", val.bg >> 8);
                        let titlebar = format!("{:06x}", val.window >> 8);
                        let style_class = key.to_lowercase().replace(" ", "-");

                        stylesheet
                            .replace("{{colorscheme}}", &style_class)
                            .replace("{{tab-active}}", &tab)
                            .replace("{{titlebar}}", &titlebar)
                    })
                    .collect::<Vec<String>>()
                    .join("\n");

                Some(scheme_sheets)
            })
    }

    pub fn get_default_profile(&self) -> Option<TerminalParams> {
        let default = self
            .config
            .try_borrow()
            .or_else(|e| {
                log::error!("Config borrow failed {:?}", e);
                Err(e)
            })
            .ok()
            .and_then(|config| Some(config.default_profile.to_owned()))?;

        self.get_profile(&default)
    }

    pub fn get_profile(&self, profile_name: &str) -> Option<TerminalParams> {
        log::trace!(
            "Config strong count {:?}",
            std::rc::Rc::strong_count(&self.config)
        );
        log::trace!(
            "Config weak count {:?}",
            std::rc::Rc::weak_count(&self.config)
        );
        self.config
            .try_borrow()
            .or_else(|e| {
                log::error!("Config borrow failed {:?}", e);
                Err(e)
            })
            .ok()
            .and_then(|config| {
                let profile = config
                    .profiles
                    .profiles
                    .get(profile_name)
                    .map(|p| p.to_owned())?;

                let colorscheme = config
                    .colorschemes
                    .get(&profile.colorscheme)
                    .map(|c| c.to_owned())?;

                Some(TerminalParams(profile, colorscheme, config.scrollbar))
            })
    }

    pub fn get_profile_names(&self) -> Option<Vec<String>> {
        log::trace!(
            "Config strong count {:?}",
            std::rc::Rc::strong_count(&self.config)
        );
        log::trace!(
            "Config weak count {:?}",
            std::rc::Rc::weak_count(&self.config)
        );

        self.config
            .try_borrow()
            .or_else(|e| {
                log::error!("Config borrow failed {:?}", e);
                Err(e)
            })
            .ok()
            .and_then(|config| {
                let profiles = config
                    .profiles
                    .profiles
                    .iter()
                    .map(|(key, _)| key.to_owned())
                    .collect::<Vec<String>>();

                Some(profiles)
            })
    }

    pub fn get_default_profile_name(&self) -> Option<String> {
        log::trace!(
            "Config strong count {:?}",
            std::rc::Rc::strong_count(&self.config)
        );
        log::trace!(
            "Config weak count {:?}",
            std::rc::Rc::weak_count(&self.config)
        );

        self.config
            .try_borrow()
            .or_else(|e| {
                log::error!("Config borrow failed {:?}", e);
                Err(e)
            })
            .ok()
            .and_then(|config| Some(config.default_profile.to_owned()))
    }

    pub fn get_layout(&self) -> Option<Layout> {
        self.config
            .try_borrow()
            .or_else(|e| {
                log::error!("Config borrow failed {:?}", e);
                Err(e)
            })
            .ok()
            .and_then(|config| Some(config.layout))
    }

    pub fn new_session(
        &self,
        profile: Option<&str>,
        cwd: Option<PathBuf>,
        cmd: Option<Vec<PathBuf>>,
    ) {
        if let Some(window) = self.window() {
            window.terminal(profile, cwd, cmd);
        }
    }
}

impl ActionGroupImpl for MtApplicationPriv {
    fn list_actions(&self, action_group: &Self::Type) -> Vec<String> {
        self.parent_list_actions(action_group)
    }

    fn query_action(
        &self,
        action_group: &Self::Type,
        action_name: &str,
    ) -> Option<(
        bool,
        Option<VariantType>,
        Option<VariantType>,
        Option<Variant>,
        Option<Variant>,
    )> {
        self.parent_query_action(action_group, action_name)
    }
}

impl ActionMapImpl for MtApplicationPriv {
    fn add_action(&self, action_map: &Self::Type, action: &Action) {
        self.parent_add_action(action_map, action);
    }

    fn remove_action(&self, action_map: &Self::Type, action_name: &str) {
        self.parent_remove_action(action_map, action_name);
    }

    fn lookup_action(&self, action_map: &Self::Type, action_name: &str) -> Option<Action> {
        self.parent_lookup_action(action_map, action_name)
    }
}

glib::wrapper! {
    pub struct MtApplication(
        ObjectSubclass<MtApplicationPriv>
    ) @extends gio::Application, gtk::Application, @implements gio::ActionMap, gio::ActionGroup;
}

impl MtApplication {
    pub fn run() {
        let app: MtApplication = glib::Object::new(&[
            ("application-id", &APPID.to_string()),
            ("flags", &ApplicationFlags::HANDLES_COMMAND_LINE),
        ])
        .expect("Failed to create MtApplication");

        app.set_default();
        app.set_resource_base_path(Some("/com/gitlab/miridyan/Mt"));

        app.add_main_option(
            "config",
            Char::from(b'c'),
            OptionFlags::NONE,
            OptionArg::String,
            "Override default config path",
            Some("CONFIG_PATH"),
        );

        app.add_main_option(
            "profile",
            Char::from(b'p'),
            OptionFlags::NONE,
            OptionArg::String,
            "Set the starting profile",
            Some("PROFILE_NAME"),
        );

        app.add_main_option(
            "working-directory",
            Char::from(b'w'),
            OptionFlags::NONE,
            OptionArg::String,
            "Set the working directory of a session",
            Some("DIRECTORY"),
        );

        app.add_main_option(
            "command",
            Char::from(b'e'),
            OptionFlags::NONE,
            OptionArg::String,
            "Set the command of a session",
            Some("COMMAND"),
        );

        app.add_main_option(
            "new-process",
            Char::from(b'\0'),
            OptionFlags::NONE,
            OptionArg::None,
            "Start new instance of Mt process",
            None,
        );

        let args: Vec<String> = std::env::args().skip(0).collect();
        ApplicationExtManual::run_with_args(&app, &args);
    }

    pub fn setup_gactions(&self) {
        self.set_accels_for_action("win.new-tab", &["<primary><shift>t"]);
        debug!("Connecting GAction: win.new-tab");

        self.set_accels_for_action("win.close-active-tab", &["<primary><shift>w"]);
        debug!("Connecting GAction: win.close-active-tab");

        self.set_accels_for_action("win.clipboard-copy", &["<primary><shift>c"]);
        debug!("Connecting GAction: win.clipboard-copy");

        self.set_accels_for_action("win.clipboard-paste", &["<primary><shift>v"]);
        debug!("Connecting GAction: win.clipboard-paste");

        util::action(
            self,
            "quit",
            clone!(@weak self as app => move |_, _| {
                app.quit();
            }),
            None,
        );
        self.set_accels_for_action("app.quit", &["<primary><shift>q"]);
        debug!("Connecting GAction: app.quit");

        util::action(
            self,
            "about",
            clone!(@weak self as app => move |_, _| {
                if let Some(window) = app.window() {
                    window::about_dialog(&window, &app);
                }
            }),
            None,
        );
        debug!("Connecting GAction: app.about");
    }

    pub fn window(&self) -> Option<MtApplicationWindow> {
        self.windows()
            .first()
            .and_then(|win| win.clone().downcast::<MtApplicationWindow>().ok())
    }

    pub fn get_default_profile(&self) -> Option<TerminalParams> {
        let app_priv = MtApplicationPriv::from_instance(self);
        app_priv.get_default_profile()
    }

    pub fn get_profile(&self, profile_name: &str) -> Option<TerminalParams> {
        let app_priv = MtApplicationPriv::from_instance(self);
        app_priv.get_profile(profile_name)
    }

    pub fn get_default_profile_name(&self) -> Option<String> {
        let app_priv = MtApplicationPriv::from_instance(self);
        app_priv.get_default_profile_name()
    }

    pub fn get_profile_names(&self) -> Option<Vec<String>> {
        let app_priv = MtApplicationPriv::from_instance(self);
        app_priv.get_profile_names()
    }

    pub fn get_layout(&self) -> Option<Layout> {
        let app_priv = MtApplicationPriv::from_instance(self);
        app_priv.get_layout()
    }
}
