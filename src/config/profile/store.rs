use super::{
    defaults::{Defaults, PROFILE_DEFAULTS},
    Profile,
};
use serde::{
    de::{self, MapAccess, SeqAccess, Visitor},
    Deserialize, Deserializer, Serialize,
};
use std::{collections::HashMap, fmt};

#[derive(Clone, Debug, PartialEq, Serialize)]
pub struct Store {
    defaults: Defaults,
    pub profiles: HashMap<String, Profile>,
}

impl Default for Store {
    fn default() -> Self {
        let default = PROFILE_DEFAULTS.get_or_init(|| Defaults::default());
        let mut profiles = HashMap::new();
        profiles.insert(String::from("Default"), Profile::default());

        Self {
            defaults: default.to_owned(),
            profiles: profiles,
        }
    }
}

impl<'de> Deserialize<'de> for Store {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[derive(Deserialize)]
        #[serde(field_identifier, rename_all = "lowercase")]
        enum Field {
            Defaults,
            Profiles,
        }
        struct StoreVisitor;

        impl<'de> Visitor<'de> for StoreVisitor {
            type Value = Store;

            fn expecting(&self, formatter: &mut fmt::Formatter<'_>) -> fmt::Result {
                formatter.write_str("struct Store")
            }

            fn visit_seq<V>(self, mut seq: V) -> std::result::Result<Self::Value, V::Error>
            where
                V: SeqAccess<'de>,
            {
                let defaults = seq
                    .next_element()?
                    .ok_or_else(|| de::Error::invalid_length(0, &self))?;
                let profiles = seq.next_element()?.unwrap_or(HashMap::new());
                // .ok_or_else(|| de::Error::invalid_length(1, &self))?;
                Ok(Store { defaults, profiles })
            }

            fn visit_map<V>(self, mut map: V) -> std::result::Result<Self::Value, V::Error>
            where
                V: MapAccess<'de>,
            {
                let mut defaults = None;
                let mut profiles = None;

                while let Some(key) = map.next_key()? {
                    match key {
                        Field::Defaults => {
                            if defaults.is_some() {
                                return Err(de::Error::duplicate_field("default"));
                            }
                            defaults = Some(map.next_value::<Defaults>()?);
                            if let Err(e) = PROFILE_DEFAULTS.set(defaults.clone().unwrap()) {
                                log::info!("{:?}", e);
                            }
                        }
                        Field::Profiles => {
                            if profiles.is_some() {
                                return Err(de::Error::duplicate_field("profiles"));
                            }
                            profiles = Some(map.next_value()?);
                        }
                    }
                }

                let defaults = defaults
                    .or(Some(Defaults::default()))
                    .ok_or_else(|| de::Error::missing_field("default"))?;
                let profiles = profiles
                    .or(Some(HashMap::new()))
                    .ok_or_else(|| de::Error::missing_field("profiles"))?;

                Ok(Store { defaults, profiles })
            }
        }

        const FIELDS: &'static [&'static str] = &["default", "profiles"];
        deserializer.deserialize_struct("Store", FIELDS, StoreVisitor)
    }
}
