mod colorscheme;
mod enums;
mod keyboard;
mod profile;

pub use self::{colorscheme::ColorScheme, profile::Profile};

pub use self::enums::*;
use crate::{error::*, traits::*};

use ron::de::from_reader;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, fs::File, path::PathBuf};

#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Config {
    #[serde(default = "Config::default_profile")]
    pub default_profile: String,

    #[serde(default)]
    pub layout: Layout,

    #[serde(default)]
    pub scrollbar: ScrollbarVisibility,

    #[serde(default)]
    session_behavior: SessionBehavior,

    #[serde(default)]
    theme: Theme,

    #[serde(default)]
    pub interior_padding: u8,

    #[serde(default)]
    pub colorschemes: HashMap<String, ColorScheme>,

    #[serde(default)]
    pub profiles: profile::Store,
}

impl Config {
    fn default_profile() -> String {
        String::from("Default")
    }
}

impl Default for Config {
    fn default() -> Self {
        let mut colorschemes = HashMap::new();
        colorschemes.insert(String::from("Base16 Dark Default"), ColorScheme::default());
        Self {
            default_profile: Config::default_profile(),
            layout: Default::default(),
            scrollbar: Default::default(),
            session_behavior: Default::default(),
            theme: Default::default(),
            interior_padding: 0,
            colorschemes,
            profiles: Default::default(),
        }
    }
}

impl Load for Config {
    fn load_from_path(path: &PathBuf) -> Result<Self> {
        let file = File::open(path)?;
        // Yes, I could use `?`, but I want to log stuff
        from_reader(file)
            .map_err(|err| {
                log::error!("Unable to load profile {:?}: {:?}", path, err);
                err.into()
            })
            .and_then(|mut conf: Self| {
                if !conf.profiles.profiles.contains_key(&conf.default_profile) {
                    conf.profiles
                        .profiles
                        .insert(conf.default_profile.clone(), profile::Profile::default());
                }
                Ok(conf)
            })
    }
}
