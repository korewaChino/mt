#[derive(Copy, Clone, Debug, Deserialize, Serialize)]
pub enum Actions {
    // if you actually have more than 128 tabs and you want to submit
    // a bug report to change this to i16, why tf do you need a shortcut
    // to go to tab 128? Like seriously, what could you possibly need that
    // for?
    SwitchToTab(i8),
    SwitchNextTab,
    SwitchPreviousTab,

    CloseActiveTab,
    CloseTabsToLeft,
    CloseTabsToRight,
    CloseActiveWindow,
}
