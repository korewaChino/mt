//! Will replace this entire set of widgets when I can use the libhandy tabview

use glib::{prelude::*, signal::SignalHandlerId, subclass, subclass::prelude::*, Object};
use gtk::{
    prelude::*, subclass::prelude::*, Align, Button, ButtonBuilder, ImageBuilder, Label,
    LabelBuilder, Orientation, PackType, ReliefStyle,
};
use once_cell::unsync::OnceCell;
use pango::EllipsizeMode;

#[derive(Clone, Debug)]
pub struct MtTabHeaderPriv {
    widgets: OnceCell<Widgets>,
}

#[derive(Clone, Debug)]
struct Widgets {
    button: Button,
    label: Label,
}

#[glib::object_subclass]
impl ObjectSubclass for MtTabHeaderPriv {
    const NAME: &'static str = "MtTabHeader";

    type Type = MtTabHeader;
    type ParentType = gtk::Box;
    type Class = subclass::basic::ClassStruct<Self>;
    type Instance = subclass::basic::InstanceStruct<Self>;

    fn new() -> Self {
        Self {
            widgets: OnceCell::new(),
        }
    }
}

impl ObjectImpl for MtTabHeaderPriv {
    fn constructed(&self, obj: &Self::Type) {
        self.parent_constructed(obj);

        let self_ = obj.downcast_ref::<MtTabHeader>().unwrap();
        let icon = ImageBuilder::new()
            .can_default(false)
            .can_focus(false)
            .icon_name("window-close-symbolic")
            // .icon_size(IconSize::Menu)
            .build();

        let button = ButtonBuilder::new()
            .can_focus(false)
            .can_default(false)
            .always_show_image(true)
            .relief(ReliefStyle::None)
            .valign(Align::Center)
            .halign(Align::Center)
            .image(&icon)
            .build();

        let label = LabelBuilder::new()
            .can_focus(false)
            .can_default(false)
            .ellipsize(EllipsizeMode::End)
            .hexpand(true)
            .label("")
            .build();

        self_.set_center_widget(Some(&label));
        self_.add(&button);
        self_.set_child_packing(&button, false, false, 0, PackType::End);

        icon.set_visible(true);
        button.set_visible(true);
        label.set_visible(true);

        button.style_context().add_class("tab-close-button");

        let widgets = Widgets { button, label };

        self.widgets.set(widgets).expect("Failed to set widgets");
    }
}

impl MtTabHeaderPriv {
    pub fn close_button(&self) -> Option<&Button> {
        if let Some(widgets) = self.widgets.get() {
            return Some(&widgets.button);
        }

        None
    }

    pub fn label(&self) -> Option<&Label> {
        if let Some(widgets) = self.widgets.get() {
            return Some(&widgets.label);
        }

        None
    }
}

impl WidgetImpl for MtTabHeaderPriv {}
impl ContainerImpl for MtTabHeaderPriv {}
impl BoxImpl for MtTabHeaderPriv {}

glib::wrapper! {
    /// This is the `Header` widget. It contains the title and the close button for the tab.
    pub struct MtTabHeader(
        ObjectSubclass<MtTabHeaderPriv>
    ) @extends gtk::Widget, gtk::Container, gtk::Box;

    // match fn {
    //     get_type => || MtTabHeaderPriv::get_type().to_glib(),
    // }
}

impl MtTabHeader {
    pub fn new() -> Self {
        let box_: MtTabHeader = Object::new(&[
            ("can-default", &false),
            ("can-focus", &false),
            ("hexpand", &true),
            ("orientation", &Orientation::Horizontal),
            ("spacing", &6),
        ])
        .expect("Failed to create MtTabHeader");
        // .downcast::<MtTabHeader>()
        // .expect("Created MtTabHeader is of wrong type");

        box_
    }

    /// Execute the closure `f` when the close button on the tab header is clicked
    pub fn connect_property_tab_close<F: Fn(&Button) + 'static>(&self, f: F) -> SignalHandlerId {
        let priv_ = MtTabHeaderPriv::from_instance(self);
        let button = priv_
            .close_button()
            .expect("Failed to get close button from MtTabHeader");

        button.connect_clicked(f)
    }

    /// Set the title of the tab header
    pub fn set_title(&self, title: &str) {
        let priv_ = MtTabHeaderPriv::from_instance(self);

        if let Some(label) = priv_.label() {
            label.set_text(title);
        }
    }
}
