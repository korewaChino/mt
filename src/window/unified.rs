use crate::{widget::menu::MtMenu, window::WindowContent};
use glib::clone;
use gtk::{prelude::*, Box, Builder, Label, MenuButton};
use libhandy::{HeaderBar, TabBar, TabView, WindowHandle};

#[derive(Debug)]
pub struct UnifiedWindowContent {
    root: Box,
    // I forget why I store this, but I think I need it.
    left_titlebar: HeaderBar,
    left_windowhandle: WindowHandle,

    right_titlebar: HeaderBar,
    right_windowhandle: WindowHandle,

    tab_bar: TabBar,
    tab_view: TabView,

    menu_button: MenuButton,
    window_title: Label,

    builder: Builder,
}

impl UnifiedWindowContent {
    pub fn new() -> Self {
        let builder =
            Builder::from_resource("/com/gitlab/miridyan/Mt/ui/unified-window-content.ui");

        let root = builder
            .object::<Box>("root-widget")
            .expect("Failed to load root-widget");
        let left_titlebar = builder
            .object::<HeaderBar>("left-headerbar")
            .expect("Failed to load left-headerbar");
        let left_windowhandle = builder
            .object::<WindowHandle>("left-windowhandle")
            .expect("Failed to load left-windowhandle");
        let right_titlebar = builder
            .object::<HeaderBar>("right-headerbar")
            .expect("Failed to load right-headerbar");
        let right_windowhandle = builder
            .object::<WindowHandle>("right-windowhandle")
            .expect("Failed to load right-windowhandle");

        let tab_bar = builder
            .object::<TabBar>("tab-bar")
            .expect("Failed to load tab-bar");
        let tab_view = builder
            .object::<TabView>("tab-view")
            .expect("Failed to load tab-view");

        let menu_button = builder
            .object::<MenuButton>("tab-button")
            .expect("Failed to load tab-button");
        let window_title = builder
            .object::<Label>("window-title")
            .expect("Failed to load window-title");

        tab_bar.connect_tabs_revealed_notify(
            clone!(@weak left_windowhandle, @weak right_titlebar, @weak window_title => move |tab_bar| {
                let revealed_state = tab_bar.is_tabs_revealed();

                tab_bar.set_visible(revealed_state);
                left_windowhandle.set_visible(revealed_state);
                right_titlebar.set_hexpand(!revealed_state);
                window_title.set_visible(!revealed_state);

                let right_titlebar_style_context = right_titlebar.style_context();

                if !revealed_state {
                    right_titlebar_style_context.add_class("secondary");
                } else {
                    right_titlebar_style_context.remove_class("secondary");
                }
            })
        );

        Self {
            root,
            left_titlebar,
            left_windowhandle,
            right_titlebar,
            right_windowhandle,
            tab_bar,
            tab_view,
            menu_button,
            window_title,
            builder,
        }
    }
}

impl WindowContent for UnifiedWindowContent {
    fn tabview(&self) -> &TabView {
        &self.tab_view
    }

    fn tabbar(&self) -> &TabBar {
        &self.tab_bar
    }

    fn set_menu(&self, menu: &MtMenu) {
        self.menu_button.set_popup(Some(&menu.into_menu()));
    }

    fn root(&self) -> &Box {
        &self.root
    }

    fn set_title(&self, title: &str) {
        self.window_title.set_text(title);
    }

    fn set_style(&self, old: &str, new: &str) {
        let right_titlebar_style_context = self.right_titlebar.style_context();
        let left_titlebar_style_context = self.left_titlebar.style_context();
        let tab_bar_style_context = self.tab_bar.style_context();

        right_titlebar_style_context.remove_class(old);
        left_titlebar_style_context.remove_class(old);
        tab_bar_style_context.remove_class(old);

        right_titlebar_style_context.add_class(new);
        left_titlebar_style_context.add_class(new);
        tab_bar_style_context.add_class(new);
    }
}
