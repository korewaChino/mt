# ![](./data/icons/banner.svg)

## Screenshots

![neofetch](./data/icons/scrot0.png)
![config](./data/icons/scrot1.png)

## General Update

Alright so apparently people are watching this now. I wasn't expecting that. Trust me, 
a lot of stuff is happening, it's just in the backend where you can't see it. Work will
continue, but it will be slow. I've been slowly changing the internals to be more flexible,
so hopefully additions will be easier to make in the future.

By the way, there is a config (you can see it in the screenshot above), so if you're alright
with using just a single profile it's actually pretty usable. I use it every day. I've heard
some of your feedback and I'm taking your suggestions into consideration. Stay tuned.